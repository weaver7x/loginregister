package practica2ipc.controlnview;

import practica2ipc.model.LoginModel;
import practica2ipc.model.RegisterModel;
import practica2ipc.model.ValidLogin;

/**
 * Controlador de la vista principal de la aplicación.
 *
 *
 * @author Cristian Tejedor García
 * @author Ismael José Taboada Rodero
 * @since 1.0
 * @version 1.0
 */
public class IndexController {

    /**
     * Vista`principal de la aplciación.
     */
    private IndexView indexView;
    /**
     * Modelo del login de usuario.
     */
    private LoginModel model;

    /**
     * Constructor con vista y modelo.
     *
     * @param v Vista del controlador.
     * @param m Modelo del controlador.
     */
    public IndexController(final IndexView v,final LoginModel m) {
        indexView = v;
        model = m;
    }

    /**
     * Muestra otra vista de recordatorio de contraseña. Elimina la vista
     * actual.
     */
    public void showRememberPassword() {
        // Cierra y elimina la ventana actual
        this.indexView.dispose();
        // Crea la nueva ventana y le pasa el model
        new RememberPasswordView(model).setVisible(true);

    }

    /**
     * Muestra otra vista de recordatorio de nombre de usuario. Elimina la vista
     * actual.
     */
    public void showRememberUserName() {
        // Cierra y elimina la ventana actual
        this.indexView.dispose();
        // Crea la nueva ventana y le pasa el model
        new RememberUserNameView().setVisible(true);

    }

    /**
     * Muestra otra vista de registro de usuario. Elimina la vista actual.
     */
    public void showRegisterForm() {
        // Cierra y elimina la ventana actual
        this.indexView.dispose();
        //Crea la nueva ventana y le pasa el model
        new RegisterFormView(new RegisterModel()).setVisible(true);
    }

    /**
     * Muestra la vista de login correcto en caso de que exista el nombre de
     * usuario y la contraseña indicaos en la vista por el ususario.
     */
    public void showConfirmLogin() {
        // 1. Borra posible error
        this.indexView.clearLoginError();
        // 2. Comprueba que exista el usuario y comprueba que concuerde la contraseña con el usuario
        boolean showError = false;
        final String inputName = this.indexView.getUserName();
        if (inputName.compareTo(ValidLogin.getValidUserName()) == 0) {
            final char[] pass = this.indexView.getPassword();
            final String inputPass = new String(pass);
            if (pass.length > 0
                    && inputPass.compareTo(ValidLogin.getLoginPassword()) == 0) {
                this.indexView.dispose();
                // 2.1. Se guarda en el model
                this.model.setUserName(inputName);
                this.model.setPassword(inputPass);
                // 2.2. Crea la nueva ventana y le pasa el model
                new ConfirmLoginView(model).setVisible(true);
            } else {
                showError = true;
            }
        } else {
            showError = true;
        }
        // 3. Si hubo error se muestra
        if (showError) {
            this.indexView.showLoginError();
        }
    }
}