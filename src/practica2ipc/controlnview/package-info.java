/**
 * <p>
 * Paquete que contiene las vistas de la interfaz gráfica y los controladoresde
 * la aplicación Practica2IPC creada para la asignatura Interacción Persona
 * Computadora del Grado en Ingeniería Informática de Sistemas en la Universidad
 * de Valladolid.
 * </p>
 *
 * <p>
 * La interfaz grafica esta diseñada con el editor gráfico de Java Swing en
 * NetBeans.
 * </p>
 *
 *
 * @author Cristian Tejedor García
 * @author Ismael José Taboada Rodero
 * @since 1.0
 * @version 1.0
 */
package practica2ipc.controlnview;