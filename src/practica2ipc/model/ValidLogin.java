package practica2ipc.model;

/**
 * Almacena los únicos nombre de usuario, contraseña y e-mail correctos para el
 * login de la aplicación.
 *
 * Como el objetivo de la misma no es implementar una persistencia, se opta por
 * esta manera de simular un login correcto.
 *
 * @author Cristian Tejedor García
 * @author Ismael José Taboada Rodero
 * @since 1.0
 * @version 1.0
 */
public final class ValidLogin {

    /**
     * Nombre de usuario correcto para el login.
     */
    private final static String LOGIN_USER_NAME = "donjose";
    /**
     * Contraseña correcta para el login.
     */
    private final static String LOGIN_PASSWORD = "1soypepe!";
    /**
     * E-mail correcta para el login.
     */
    private final static String LOGIN_EMAIL = "joslop@gmail.com";

    /**
     * Obtiene el único nombre de usuario válido de la aplicación.
     *
     * @return El único nombre de usuario válido de la aplicación.
     */
    public static String getValidUserName() {
        return LOGIN_USER_NAME;
    }

    /**
     * Obtiene la única contraseña válida de la aplicación.
     *
     * @return La única contraseña válida de la aplicación.
     */
    public static String getLoginPassword() {
        return LOGIN_PASSWORD;
    }

    /**
     * Obtiene el único e-mail válido de la aplicación.
     *
     * @return El único e-mail válido de la aplicación.
     */
    public static String getValidEmail() {
        return LOGIN_EMAIL;
    }
}