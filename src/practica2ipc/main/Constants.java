package practica2ipc.main;

/**
 * Clase con las contantes utilizadas en la apliación.
 *
 * @author Cristian Tejedor García
 * @author Ismael José Taboada Rodero
 * @since 1.0
 * @version 1.0
 */
public final class Constants {

    /**
     * Máxima longitud del campo de nombre de usuario.
     */
    public static final byte MAX_LENGTH_USER_NAME = 15;
    /**
     * Máxima longitud del campo de contraseña.
     */
    public static final byte MAX_LENGTH_PASSWORD = 20;
    /**
     * Máxima longitud del campo de e-mail.
     */
    public static final byte MAX_LENGTH_EMAIL = 100;
    /**
     * Máxima longitud del campo de nombre.
     */
    public static final byte MAX_LENGTH_NAME = 30;

    /**
     * Patrón del formato sin longitud del nombre de usuario.
     */
    public final static String USER_NAME_FORMAT_NO_LENGTH = "[A-Za-z0-9]";
    /**
     * Patrón del formato sin longitud de la contraseña.
     */
    public final static String PASSWD_FORMAT_NO_LENGTH = "[A-Za-z0-9&'!*+-./=?_~]";
    /**
     * Patrón del formato sin longitud del correo electrónico.
     */
    public final static String EMAIL_FORMAT_NO_LENGTH = "[A-Za-z0-9.@_%+-]";
    /**
     * Patrón del formato sin longitud del nombre.
     */
    public final static String NAME_FORMAT_NO_LENGTH = "[A-Za-z]";

    /**
     * Patrón del formato del nombre de usuario.
     */
    public final static String USER_NAME_FORMAT = USER_NAME_FORMAT_NO_LENGTH+"{6," + MAX_LENGTH_USER_NAME + "}";
    /**
     * Patrón del formato de la contraseña.
     */
    public final static String PASSWD_FORMAT = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[&'!*+-./=?_~]).{6,"+MAX_LENGTH_PASSWORD+"})";
    /**
     * Patrón del formato del correo electrónico.
     */
    public final static String EMAIL_FORMAT = "^[\\w-]+(\\.[\\w-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    /**
     * Patrón del formato del nombre.
     */
    public final static String NAME_FORMAT = NAME_FORMAT_NO_LENGTH+"{0,"+MAX_LENGTH_NAME+"}";
}