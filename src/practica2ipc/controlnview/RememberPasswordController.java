package practica2ipc.controlnview;

import practica2ipc.model.ValidLogin;

/**
 * Controlador de la vista de recordatorio de ncontraseña de la aplicación.
 *
 *
 * @author Cristian Tejedor García
 * @author Ismael José Taboada Rodero
 * @since 1.0
 * @version 1.0
 */
public class RememberPasswordController {

    /**
     * Vista a gestionar.
     */
    private RememberPasswordView rememberView;

    /**
     * Constructor del controlador con la vista a gestionar.
     *
     * @param v Vista del controlador
     */
    public RememberPasswordController(final RememberPasswordView v) {
        rememberView = v;
    }

    /**
     * Avanza al siguiente paso (vista) si los datos existen. Elimina la vista
     * actual.
     */
    public void showNextStep() {
        if (checkData()) {
            // Cierra y elimina la ventana actual
            this.rememberView.dispose();
            // Crea la nueva ventana y le pasa el model
            new SentInformationView().setVisible(true);
        }

    }

    /**
     * Vuelve a la vista anterior. Elimina la vista actual.
     */
    public void showBackView() {
        // Cierra y elimina la ventana actual
        this.rememberView.dispose();
        // Crea la nueva ventana y le pasa el model
        new IndexView().setVisible(true);
    }

    /**
     * Comprueba que exista el nombre de usuario o el correo electrónico.
     */
    private boolean checkData() {
        boolean isValid = false;
        this.rememberView.clearRememberError();
        final String input = this.rememberView.getUserNameEmail();
        if (input != null && !input.isEmpty() && (ValidLogin.getValidUserName().
                compareTo(input) == 0 || ValidLogin.getValidEmail().compareTo(input) == 0)) {
            isValid = true;
        } else {
            this.rememberView.showRememberError();
        }
        return isValid;
    }
}