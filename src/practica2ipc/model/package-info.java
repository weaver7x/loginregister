/**
 * <p>
 * Paquete que contiene el modelo (simulado) de la aplicación Practica2IPC
 * creada para la asignatura Interacción Persona Computadora del Grado en
 * Ingeniería Informática de Sistemas en la Universidad de Valladolid.
 * </p>
 *
 * <p>Es simulado porque el objetivo no es implementar una persistencia real
 * para esta práctica.</p>
 *
 * @author Cristian Tejedor García
 * @author Ismael José Taboada Rodero
 * @since 1.0
 * @version 1.0
 */
package practica2ipc.model;