/**
 * <p>
 * Paquete que contiene clases de utilidad para la aplicación Practica2IPC
 * creada para la asignatura Interacción Persona Computadora del Grado en
 * Ingeniería Informática de Sistemas en la Universidad de Valladolid.
 * </p>
 *
 * <p>Se encuentra un Captcha personalizado para el registro de nuevos
 * usuarios.</p>
 *
 * @author Cristian Tejedor García
 * @author Ismael José Taboada Rodero
 * @since 1.0
 * @version 1.0
 */
package practica2ipc.utils;