package practica2ipc.controlnview;

import practica2ipc.model.LoginModel;
import practica2ipc.model.RegisterModel;
import practica2ipc.model.ValidLogin;

/**
 * Controlador de la vista del registro de la apliación.
 *
 *
 * @author Cristian Tejedor García
 * @author Ismael José Taboada Rodero
 * @since 1.0
 * @version 1.0
 */
public class RegisterFormController {

    /**
     * Atributo de la vista de registro respectiva del controlador.
     */
    private RegisterFormView registerView;
    /**
     * Atributo de la vista de términos y condiciones respectia del controlador.
     */
    private TermsNConditionsView termsView;
    /**
     * Atribudo del modelo de registro respectivo del controlador y la vista.
     */
    private RegisterModel model;

    /**
     * Constructor del controlador del registro
     *
     * @param r Vista del registro
     * @param m Modelo del registro
     */
    public RegisterFormController(final RegisterFormView r, final RegisterModel m) {
        registerView = r;
        model = m;
    }

    /**
     * Método que muestra la vista de Términos y Condiciones del sistema,
     * después de validar que los datos obtenidos del formulario de registro
     * tengan formato correcto.
     */
    public void showTermsNConditions() {
        boolean valid;
        // Se borran los posibles mensajes de error de la vista
        this.registerView.clearErrors();
        // Se piden los datos del formulario a la vista
        valid = this.takeInformation();
        // Si los datos son válidos se crea una nueva vista de términos y
        //condiciones y la muestra
        if (valid) {
            termsView = new TermsNConditionsView(this);
            this.termsView.showSelf();
        }
    }

    /**
     * Método que cambia la vista actual a la principal (Index) de la
     * aplicación.
     */
    public void showIndex() {
        // Cierra y elimina la ventana actual
        this.registerView.dispose();
        // Crea la nueva ventana y le pasa el modelo
        new IndexView(new LoginModel()).setVisible(true);

    }

    /**
     * Método que cambia la vista actual a la de confirmación de registro.
     */
    public void showConfirm() {
        // Cierra y elimina las ventanas actual
        this.registerView.dispose();
        this.termsView.dispose();
        // Crea la nueva ventana y le pasa el modelo
        new SentInformationView().setVisible(true);
    }

    /**
     * Método que llama a la vista de Términos y Condiciones para que aparezca
     * sobre la de Registro.
     */
    public void showRegisterForm() {
        //Elimina ventana de Términos y condiciones
        this.termsView.dispose();
    }

    /**
     * Método que toma los datos del formulario de registro, los introduce en el
     * modelo, informa a la vista de Registro si hubiese algún error.
     *
     * @return devuelve valor true si todos los dátos son válidos, en caso
     * contrario devuelve un false.
     */
    public boolean takeInformation() {
        boolean validUser, validMail, validPasswd, validName, validConfirmMail,
                validConfirmPass, validCaptcha;
        //Valida la respuesta al captcha del usuario
        validCaptcha = this.registerView.getAnswerUser().
                compareToIgnoreCase(this.registerView.getAnswer()) == 0;
        if (!validCaptcha) {
            this.registerView.showCaptchaError();
        }

        //Valida el formato del nombre de usuario
        final String inputUserName = this.registerView.getUserName();
        validUser = (inputUserName.compareTo(ValidLogin.getValidUserName())
                != 0)
                && this.model.setUserName(inputUserName);
        if (!validUser) {
            this.registerView.showUserError();
        };
        //Valida el formato y la confirmación del correo electrónico
        final String inputMail = this.registerView.getMail();
        validMail = this.model.setMail(inputMail);
        if (!validMail) {
            this.registerView.showMailError();
        }
        validConfirmMail = !inputMail.isEmpty() && this.registerView.
                getConfirmMail().
                compareTo(inputMail) == 0;
        if (!validConfirmMail) {
            this.registerView.showConfirmMailError();
        }
        //Valida el formato y la confirmación de la contraseña
        validPasswd = this.model.setPasswd(this.registerView.getPasswd());
        if (!validPasswd) {
            this.registerView.showPasswdError();
        }
        final String inputPasswd = this.registerView.getConfirmPasswd();
        validConfirmPass = !inputPasswd.isEmpty() && inputPasswd.
                compareTo(this.registerView.getPasswd())
                == 0;
        if (!validConfirmPass) {
            this.registerView.showConfirmPasswdError();
        }
        //Valida el formato del nombre
        validName = this.model.setName(this.registerView.getNormalName());
        if (!validName) {
            this.registerView.showNameError();
        }

        return validUser && validMail && validPasswd && validName
                && validConfirmMail && validConfirmPass && validCaptcha;

    }
}