package practica2ipc.controlnview;

import practica2ipc.model.LoginModel;

/**
 * Controlador vista de la ventana de confirmación para las acciones de 
 * {@link RememberPasswordController}, {@link RememberUserNameController} y 
 * {@link RegisterFormController} de la aplicación.
 *
 * @author Cristian Tejedor García
 * @author Ismael José Taboada Rodero
 * @since 1.0
 * @version 1.0
 */
public class SentInformationController {

    /**
     * Atributo de la vista del controlador.
     */
    private SentInformationView rememberView;

    /**
     * Constructor del controlador
     * 
     * @param v vista correspondiente del controlador
     */
    public SentInformationController(SentInformationView v) {
        rememberView = v;

    }

    /**
     * Método que cambia la vista actual a la vista {@link IndexView} de 
     * la aplicación.
     */
    public void showNextStep() {
        // Cierra y elimina la ventana actual
        this.rememberView.dispose();
        // Crea la nueva ventana y le pasa el modelo
        new IndexView(new LoginModel()).setVisible(true);

    }
}