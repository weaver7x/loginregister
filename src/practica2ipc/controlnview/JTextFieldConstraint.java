package practica2ipc.controlnview;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

/**
 * Establece un límite de longitud de texto en JTextFields. Además impide
 * introducir caracteres invalidos según el patrón de caracteres que se le
 * especifique.
 *
 * @author Cristian Tejedor García
 * @author Ismael José Taboada Rodero
 * @since 1.0
 * @version 1.0
 */
public class JTextFieldConstraint extends PlainDocument {

    /*
     * Límite de la longitud del campo.
     */
    private int limit;
    /**
     * Patrón del campo de caracteres.
     */
    private String pattern;

    /**
     * Constructor que inicializa la longitud máxima del campo y el patrón a
     * seguir por los caracteres del campo de texto.
     *
     * @param limit Límite de la longitud del campo.
     * @param pattern Patrón correcto a seguir por los caracteres en el campo de
     * texto.
     */
    public JTextFieldConstraint(final int limit, final String pattern) {
        super();
        this.limit = limit;
        this.pattern = pattern;
    }

    /**
     *
     * @param offset
     * @param str
     * @param attr
     * @throws BadLocationException
     */
    @Override
    public void insertString(final int offset, final String str, final AttributeSet attr) throws BadLocationException {
        if (str == null) {
            return;
        }
        final char[] text = str.toCharArray();
        final int SIZE_TEXT = text.length;
        String insertar;
        for (int i = SIZE_TEXT-1; i >= 0; i--) {
            insertar = String.valueOf(text[i]);
            if (insertar.matches(this.pattern) && ((getLength() + insertar.length()) <= limit)) {
                super.insertString(offset, insertar, attr);
            }
        }
    }
}