package practica2ipc.controlnview;

import practica2ipc.model.LoginModel;

/**
 * Controlador de la vista de recordatorio de contraseña de la aplicación.
 *
 *
 * @author Cristian Tejedor García
 * @author Ismael José Taboada Rodero
 * @since 1.0
 * @version 1.0
 */
public class ConfirmLoginController {

    /**
     * Vista a gestionar por el controlador.
     */
    private ConfirmLoginView confirmView;

    /**
     * Modelo de login.
     */
    private LoginModel model;

    /**
     * Constructor con la vista y el modelo de login.
     * @param v Vista a mostrar.
     * @param m Modelo de login.
     */
    public ConfirmLoginController(final ConfirmLoginView v, final LoginModel m) {
        model = m;
        confirmView = v;

    }

    /**
     * Muestra la vista principal de la palicación.
     * Elimina la vista actual.
     */
    public void showIndex() {
        // Borra elmodel actual
        this.model.setUserName("");
        this.model.setPassword("");
        // Cierra y elimina la ventana actual
        this.confirmView.dispose();
        // Crea la nueva ventana y le pasa el model
        new IndexView(model).setVisible(true);

    }
}