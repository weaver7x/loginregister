/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package practica2ipc.model;

import practica2ipc.main.Constants;

/**
 * Modelo delformulario de registro de la aplicación.
 *
 * @author Cristian Tejedor García
 * @author Ismael José Taboada Rodero
 * @since 1.0
 * @version 1.0
 */
public class RegisterModel {
    
    /**
     * Atributo de nombre de usuario.
     */
    private String username;
    /**
     * Atributo de correo electrónico.
     */
    private String mail;
    /**
     * Atributo de contraseña.
     */
    private String passwd;
    /**
     * Atributo de nombre.
     */
    private String name;

    /**
     * Método que guarda el nombre de usuario en el modelo desde el {@link practica2ipc.controlnview.RegisterFormController}
     * 
     * @param s valor del string del nombre de usuario
     * 
     * @return devuelve true si la variable de parámetro tiene formáto válido,
     * false en caso contrario
     */
    public boolean setUserName(String s) {
        boolean valid = validateUserName(s);
        if(valid)
            this.username = s;
        return valid;
    }

    /**
     * Método que guarda el correo electrónico en el modelo desde el {@link practica2ipc.controlnview.RegisterFormController}
     * 
     * @param s valor del string del correo electrónico
     * 
     * @return devuelve true si la variable de parámetro tiene formáto válido,
     * false en caso contrario
     */
    public boolean setMail(String s) {
        boolean valid = validateMail(s);
        if(valid)
            this.mail = s;
        return valid;
    }

    /**
     * Método que guarda la contraseña en el modelo desde el {@link practica2ipc.controlnview.RegisterFormController}
     * 
     * @param s valor del string de la contraseña
     * 
     * @return devuelve true si la variable de parámetro tiene formáto válido,
     * false en caso contrario
     */
    public boolean setPasswd(String s) {
        boolean valid = validatePasswd(s);
        if(valid)
            this.passwd = s;
        return valid;
    }

    /**
     * Método que guarda el nombre en el modelo desde el {@link practica2ipc.controlnview.RegisterFormController}
     * 
     * @param s valor del string del nombre
     * 
     * @return devuelve true si la variable de parámetro tiene formáto válido,
     * false en caso contrario
     */
    public boolean setName(String s) {
        boolean valid = validateName(s);
        if(valid)
            this.name = s;
        return valid;
    }

    /**
     * Método que valida el formato del nombre de usuario con el patron 
     * contenido en {@link practica2ipc.main.Constants }.
     * 
     * @param s valor del nombre de usuario
     *
     * @return true si el parámetro tiene el formato adecuado, false en caso
     * contrario
     */
    private boolean validateUserName(String s){
        return s.matches(Constants.USER_NAME_FORMAT)&&(!s.isEmpty());
    }
    
    /**
     * Método que valida el formato del correo electrónico con el patron 
     * contenido en {@link practica2ipc.main.Constants }.
     * 
     * @param s valor del correo electrónico
     *
     * @return true si el parámetro tiene el formato adecuado, false en caso
     * contrario
     */
    private boolean validateMail(String s) {
        return s.matches(Constants.EMAIL_FORMAT)&&(!s.isEmpty());
    }

    /**
     * Método que valida el formato de la contraseña con el patron 
     * contenido en {@link practica2ipc.main.Constants }.
     * 
     * @param s valor de la contraseña
     *
     * @return true si el parámetro tiene el formato adecuado, false en caso
     * contrario
     */
    private boolean validatePasswd(String s) {
        return s.matches(Constants.PASSWD_FORMAT)&&(!s.isEmpty());
    }

    /**
     * Método que valida el formato del nombre con el patron 
     * contenido en {@link practica2ipc.main.Constants }.
     * 
     * @param s valor del nombre
     *
     * @return true si el parámetro tiene el formato adecuado, false en caso
     * contrario
     */
    private boolean validateName(String s) {
        return s.matches(Constants.NAME_FORMAT);
    }
}
