package practica2ipc.model;

/**
 * Simula un nombre de usuario y contraseñoa correctos para el login de la
 * aplciación.
 *
 * Como el objetivo de la misma no es implementar una persistencia, se opta por
 * esta manera de almacenar en forma de constantes el único login correcto.
 *
 * @author Cristian Tejedor García
 * @author Ismael José Taboada Rodero
 * @since 1.0
 * @version 1.0
 */
public final class LoginModel {

    /**
     * Nombre del usuario en el sistema.
     */
    private String userName;
    /**
     * Contraseña del usuario en el sistema.
     */
    private String password;

    /**
     * Devuelve el nombre del usuario en el sistema.
     *
     * @return El nombre del usuario en el sistema.
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Establece la contraseña del usuario en el sistema.
     *
     * @param userName Nombre de usuario a establecer.
     */
    public void setUserName(final String userName) {
        this.userName = userName;
    }

    /**
     * Devuelve la contraseña del usuario en el sistema.
     *
     * @param password La contraseña del usuario en el sistema.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Establece la contraseña del usuario en el sistema.
     *
     * @param password Contraseña del usaurio del sistema.
     */
    public void setPassword(final String password) {
        this.password = password;
    }
}