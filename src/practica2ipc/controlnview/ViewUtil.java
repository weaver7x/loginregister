package practica2ipc.controlnview;

import javax.swing.JFrame;

/**
 * Utilidad para las vistas de la aplicación, permite centrar la vista en la
 * interfaz visual de salida.
 *
  * @author Cristian Tejedor García
 * @author Ismael José Taboada Rodero
 * @since 1.0
 * @version 1.0
 */
public final class ViewUtil {

    /**
     * Centra la vista actual en la interfaz de salida.
     *
     * @param jframe Vista a centrar.
     */
    public static void centerView(final JFrame jframe) {
        jframe.setLocationRelativeTo(null);
    }
}