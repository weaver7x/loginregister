package practica2ipc.utils;

/**
 * Clase que contiene el captcha personalizado para esta aplicación.
 *
 * Esta consiste en la existencia de un array bidimensional de Strings, cuyos
 * primeros elementos guardan un par de pregunta-respuesta (QA). De tal manera
 * que sólo serán válidos cuando la respuesta introducida coincida con la
 * correspondiente del par.
 *
 * @author Cristian Tejedor García
 * @author Ismael José Taboada Rodero
 * @since 1.0
 * @version 1.0
 */
public final class CustomCaptcha {

    /**
     * Constante para identificar la variable de pregunta y respuesta dentro del
     * par pairQA.
     */
    public static final int ID_Q = 0;
    /**
     * Constante para identificar la variable de respuesta dentro del par
     * pairQA.
     */
    public static final int ID_A = 1;
    /**
     * Atributo array que guarda los pares pregunta-respuesta de la clase.
     */
    private static final String[][] pares = new String[][]{{"Suma 2 y 2:", "4"},
        {"¿Capital de España?", "Madrid"}, {"Color del caballo blanco de Santiago", "Blanco"},
        {"Cuatro menos dos (número)", "2"}, {"Primera letra de \"Biblioteca\"", "B"},
        {"Última letra de \"Veloz\"", "z"}, {"Total de letras de \"casa\" (número)", "4"}};
    /**
     * Atributo de la longitud del array de pares pregunta-respuesta.
     */
    private static final int LEN_PAIRS = pares.length;

    /**
     * Método que devuelve aleatoriamente un par pregunta-respuesta QA dentro
     * del atributo pares.
     *
     * @return Par aleatorio pregunta-respuesta (QA).
     */
    public static String[] getPair() {
        final int randomNum = (int) (Math.random() * LEN_PAIRS);
        return pares[randomNum];
    }
}